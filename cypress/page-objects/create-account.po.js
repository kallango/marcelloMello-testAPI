class CreateAccount{
  constructor(){
    this.email = 'input#email_create'
    this.submitCreate = 'button#SubmitCreate'
    this.password = 'input#passwd'
    
    this.gender  = 'input[name="id_gender"]'
    this.customerFirstName = 'input#customer_firstname'
    this.customerLastName = 'input#customer_lastname'
    
    this.days = 'select#days'
    this.months = 'select#months'
    this.years = 'select#years'

    this.firstName = 'input#firstname'
    this.lastName = 'input#lastname'
    this.company = 'input#company'
    this.address1 = 'input#address1'
    this.address2 = 'input#address2'
    this.additionalInformation = 'textarea#other'

    this.postCode = 'input#postcode'
    this.city = 'input#city'
    this.country = 'select#id_country'
    this.states = 'select#id_state'    
    
    this.phone = 'input#phone'
    this.phoneMobile = 'input#phone_mobile'

    this.addressAlias = 'input#alias'
    this.submitAccount = 'button#submitAccount'
  }
}

module.exports = new CreateAccount()